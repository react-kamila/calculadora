import React from "react";
import "./Display.css";

export default (props) => (
  <div className="display">
    <div className="scroll">{props.value}</div>
  </div>
);
