import React from 'react'
import './Button.css';

export default props => {

    let classes = 'button ';
    classes += props.double ? 'double' : '';
    classes += props.triple ? 'triple' : '';
    classes += props.color === "G" ? 'green' : '';
    classes += props.color === "R" ? 'red'  : '';
    classes += props.color === "B" ? 'blue' : '';
    classes += props.color === "O" ? 'orange' : '';

    return (
    /* Recebe uma função que chama de 'click' e envia a props.label como parametro */
    <button 
        onClick={e => props.click && props.click(props.label)}
        className={classes}>
        {props.label}
    </button>
    )
}