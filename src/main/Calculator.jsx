import React, { Component, Fragment } from 'react'
import './Calculator.css';
import Display from '../components/Display';

import Button from '../components/Button';

const initialState = {
    displayValue: '0',
    clearDisplay: false,
    operation: null,
    values: [0, 0],
    current: 0,
    color: 'O',
}


export default class Calculator extends Component {

    state = { ...initialState };
    constructor(props) {
        super(props);
        this.addDigit = this.addDigit.bind(this);
        this.clearMemory = this.clearMemory.bind(this);
        this.setOperation = this.setOperation.bind(this);
        this.changeColor = this.changeColor.bind(this);
        this.state = { ...initialState };
    }

    clearMemory() {
        this.setState({...initialState, color: this.state.color});
    }

    setOperation(operation) {
        if (this.state.current === 0) {
             // Primeira operação
            this.setState({ operation, current: 1, clearDisplay: true });
        } else {
            const equals = operation === '=';
            /* Caso já havia uma operação setada, por exemplo, 12 + 8 - 6,
             * ao digitar o menos '-' a soma deve ser realizada (12 + 8) e seu valor armazenado no value[0]
             * Essa primeira operação está armazenada em this.state.operation
             */
            const currentOperation = this.state.operation;
            const values = [...this.state.values]; // Clone dos values
            try {
                values[0] = eval(`${values[0]} ${currentOperation} ${values[1]}`); // efetuando ooperação e armazenando no value[0]
            } catch(e) {
                values[0] = this.state.values[0];
            }
            values[1] = 0;

            this.setState({
                displayValue: values[0], //mostra resultado no display
                operation: equals ? null : operation, // se teclou '=' a operacao foi finalizada e recebe null
                current: equals ? 0 : 1, // se teclou '=' efetuou a primeir aoperacao
                clearDisplay: !equals, // se teclou '=' não limpa o display
                values
            })
        }
    }

    addDigit(n) {
        if (n === '.' && this.state.displayValue.includes('.')) {
            /* se já há um ponto inserido ignorar tentativa de inserir '.' novamente */
            return
        }
        if (n === '.' && this.state.displayValue === '0') {
            /* Opcional: se houver o valor '0' no display e o usuário clicar '.' insira no display '0.' */
            n = '0.';
        }
        /* Se só tiver o dígito "0" no display e o usuário digitar novo 0 não adicionar zeros à esquerda.
         *  Inserindo apenas valores significativos: clearDisplay = true */
        const clearDisplay = this.state.displayValue === '0' || this.state.clearDisplay;
       
        // Seta valor atual do display na const currentValue se clearDisplay === false
        const currentValue = clearDisplay ? '' : this.state.displayValue;
       
        // Concatena dígito clicado "n" ao valor atual do display
        const displayValue = currentValue + n;
   
        this.setState({ displayValue, clearDisplay: false }) // atualiza estado

        if (n !== '.') {
            // Captura o índice corrente digitado, primeiro número ou último
            const i = this.state.current;
            const newValue = parseFloat(displayValue);
            const values = [...this.state.values];
            values[i] = newValue; // Insere no índice capturado o valor do display em float
            this.setState({ values });
        }
    }

    changeColor(c) {
        this.setState({ color: c });
    }

    render() {
        // const setOperation = op => this.setOperation(op);

        return (
            <Fragment>
                <div className="calculator">
                    <Display value={this.state.displayValue} />
                    <Button label="AC" click={() => this.clearMemory()} triple />
                    <Button label="/" click={this.setOperation} color={this.state.color} />
                    <Button label="8" click={this.addDigit} />
                    <Button label="7" click={this.addDigit} />
                    <Button label="9" click={this.addDigit} />
                    <Button label="*" click={this.setOperation} color={this.state.color} />
                    <Button label="4" click={this.addDigit} />
                    <Button label="5" click={this.addDigit} />
                    <Button label="6" click={this.addDigit} />
                    <Button label="-" click={this.setOperation} color={this.state.color} />
                    <Button label="1" click={this.addDigit} />
                    <Button label="2" click={this.addDigit} />
                    <Button label="3" click={this.addDigit} />
                    <Button label="+" click={this.setOperation} color={this.state.color} />
                    <Button label="0" click={this.addDigit} double />
                    <Button label="." click={this.addDigit} />
                    <Button label="=" click={this.setOperation} color={this.state.color} />
                </div>

                <div className="buttons-foot">
                    <Button label="R" click={this.changeColor} color="R" />
                    <Button label="G" click={this.changeColor} color="G" />
                    <Button label="B" click={this.changeColor} color="B" />
                    <Button label="O" click={this.changeColor} color="O" />
                </div>
            </Fragment>
        )
    }
}
